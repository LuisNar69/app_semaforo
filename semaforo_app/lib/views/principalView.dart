import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';

class PrincipalView extends StatefulWidget {
  const PrincipalView({Key? key}) : super(key: key);

  @override
  _PrincipalViewState createState() => _PrincipalViewState();
}

class _PrincipalViewState extends State<PrincipalView> {
  Future<List<String>> _listar() async {
    // FacadeService servicio = FacadeService();

    try {
      // var value = await servicio.listarAllComentarios(param);

      /* if (value.code == 200) {
        return value.data;
      } else {
        if (value.code != 200) {
          Navigator.pushNamed(context, '/principal');
        }
      }*/
    } catch (e) {
      log("Error al cargar comentarios: $e");
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.transparent,
        appBarTheme: const AppBarTheme(
          backgroundColor: Color.fromARGB(19, 0, 0, 0),
          elevation: 0,
        ),
      ),
      home: Scaffold(
        appBar: AppBar(
          title:
              const Text('Semaforo UV', style: TextStyle(color: Colors.white)),
          centerTitle: true,
          backgroundColor: Color.fromARGB(186, 0, 0, 0),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/c.jpg"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                Color.fromARGB(178, 0, 0, 0).withOpacity(0.5),
                BlendMode.colorBurn,
              ),
            ),
          ),
          child: Stack(
            fit: StackFit.expand,
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  ),
                  CardsUv(),
                  Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  ),
                  SizedBox(
                    width: 200,
                    height: 200,
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(color: Colors.black, width: 1.0),
                      ),
                      color: Color.fromARGB(149, 39, 255, 183),
                      child: const Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Radiación UV',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              "5 UV",
                              style:
                                  TextStyle(fontSize: 30, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DatosList extends StatelessWidget {
  //final Comentario coment;

  // DatosList(this.coment);
  DatosList(comment);

  @override
  Widget build(BuildContext context) {
    return Container(
      //elevation: 10,
      child: const Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            // title: Text("Usuario: ${coment.usuario} "),
            //subtitle: Text("Comentario: ${coment.texto}"),
            title: Text("Datos recolectados"),
            subtitle: Text("Datos recolectados"),
          ),
        ],
      ),
    );
  }
}

List<Color> colores = [
  Color.fromARGB(255, 100, 204, 104),
  Color.fromARGB(255, 247, 210, 47),
  Color.fromARGB(255, 255, 158, 48),
  Color.fromRGBO(255, 102, 99, 1),
  Color.fromARGB(255, 177, 131, 255)
];

List<String> nivel = [
  "Bajo: 0 a 2",
  "Moderado: 3 a 5",
  "Alto: 6 a 7",
  "Muy Alto: 8 a 11",
  "Extremadamente Alto: 11 a 15"
];

List<String> recomendacion = [
  "No existe peligro",
  "Se recomienda usar protector",
  "Se recomeinda usar protector y gorras",
  "Se recomienda cuidar su piel",
  "No salga de su casa sol peligroso"
];

class CardsUv extends StatefulWidget {
  const CardsUv({Key? key}) : super(key: key);

  @override
  _CardsUvState createState() => _CardsUvState();
}

class _CardsUvState extends State<CardsUv> {
  int currentIndex = 0;
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 4), (Timer t) {
      setState(() {
        currentIndex = (currentIndex + 1) % colores.length;
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: colores[currentIndex],
        child: SizedBox(
          width: 300,
          height: 100,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  nivel[currentIndex],
                  style: const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
                SizedBox(height: 8),
                Text(
                  recomendacion[currentIndex],
                  style: const TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0), fontSize: 15),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
